package com.foodee.verticles;


import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;


public class RestVerticle extends AbstractVerticle {

    public static final String API_V1 = "/api/v1";

    @Override
    public void start() {
        vertx.deployVerticle(YelpClientVerticle.class.getName(), new DeploymentOptions().setWorker(true));
<<<<<<< HEAD:src/main/java/com/foodee/verticles/ServerVerticle.java
=======
        vertx.deployVerticle(FactualVerticle.class.getName(), new DeploymentOptions().setWorker(true));
>>>>>>> parent of df83ef5... Database schema:src/main/java/com/foodee/verticles/RestVerticle.java
        Router router = Router.router(vertx);

        router.route().handler(BodyHandler.create());
        router.get(API_V1 + "/business").handler(this::getBusinessesWithParams);
        router.get(API_V1 + "/business/all").handler(this::getAllBusinesses);
        vertx.createHttpServer().requestHandler(router::accept).listen(4000);
    }

    private void getBusinessesWithParams(RoutingContext routingContext) {

        String term = routingContext.request().getParam("term");
        String location = routingContext.request().getParam("location");
        String limit = routingContext.request().getParam("limit");
        JsonObject o = new JsonObject();
        o.put("term",term);
        o.put("location",location);
        o.put("limit",limit);
        vertx.eventBus().send("api", o, r -> {
            System.out.println("[Main] Receiving reply ' " + r.result().body()
                    + "' in " + Thread.currentThread().getName());
            String response = (String) r.result().body();
            JsonObject obj = new JsonObject(response);
            routingContext.response().putHeader("content-type", "application/json").end(obj.encodePrettily());
        });
    }

    private void getAllBusinesses(RoutingContext routingContext) {


        vertx.eventBus().send("api", "get", r -> {
            System.out.println("[Main] Receiving reply ' " + r.result().body()
                    + "' in " + Thread.currentThread().getName());
            String response = (String) r.result().body();
            JsonObject obj = new JsonObject(response);
            routingContext.response().putHeader("content-type", "application/json").end(obj.encodePrettily());
        });
//        consumer.handler(message -> {
//            JsonObject m = new JsonObject((String) message.body());
//            System.out.println("Received message: " + message.body());
//            routingContext.response().putHeader("content-type", "application/json").end(m.encodePrettily());
//
//        });
    }
<<<<<<< HEAD:src/main/java/com/foodee/verticles/ServerVerticle.java
=======

    private void getFactualData(RoutingContext routingContext) {
        String term = routingContext.request().getParam("term");
        String limit = routingContext.request().getParam("limit");
        JsonObject o = new JsonObject();
        o.put("term",term);
        o.put("limit",limit);
        vertx.eventBus().send("api", o , r -> {
            System.out.println("[Main] Receiving reply ' " + r.result().body()
                    + "' in " + Thread.currentThread().getName());
            String response = (String) r.result().body();
            JsonObject obj = new JsonObject(response);
            routingContext.response().putHeader("content-type", "application/json").end(obj.encodePrettily());
        });
    }
>>>>>>> parent of df83ef5... Database schema:src/main/java/com/foodee/verticles/RestVerticle.java
}