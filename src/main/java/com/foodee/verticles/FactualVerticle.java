package com.foodee.verticles;

import com.factual.driver.Factual;
import com.factual.driver.Query;
import com.factual.driver.ReadResponse;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;

/**
 * Created by nghibui on 6/1/16.
 */
public class FactualVerticle extends AbstractVerticle {

    private static final String FACTUAL_KEY = "JnN4NZMHoqUVdwYAszJVnkEQX0agW1i8GPrqegZg";
    private static final String FACTUAL_SECRET = "7WZBB1LTUFBnyssmIFhOfhmXYlRuNeCbYQ7mgs78";

    @Override
    public void start() {
        Factual factual = new Factual(FACTUAL_KEY, FACTUAL_SECRET);
        vertx.eventBus().consumer("api", message -> {
            System.out.println("[Worker] Consuming data in " + Thread.currentThread().getName());
            JsonObject o = (JsonObject) message.body();
            ReadResponse response = factual.fetch("places", new Query().search(o.getString("term")).limit(Integer.valueOf(o.getString("limit"))));

            message.reply(response.getJson());
        });
    }

}
