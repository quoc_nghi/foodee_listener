package com.foodee.bootstrap;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.StaticHandler;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class RestProvider extends AbstractVerticle {
    private Map<String, JsonObject> products = new HashMap<>();

    @Override
    public void start() {

        Router router = Router.router(vertx);

        // Serve the static pages
        router.route().handler(StaticHandler.create());
        router.get("/api/v1/business/all").handler(this::handleBusinesses);

        vertx.createHttpServer().requestHandler(router::accept).listen(5000);
    }

    private void handleBusinesses(RoutingContext routingContext) {
        JsonArray arr = new JsonArray();
        products.forEach((k, v) -> arr.add(v));
        routingContext.response().putHeader("content-type", "application/json").end(arr.encodePrettily());
    }

    private void setUpInitialData() {
        addBusiness(new JsonObject().put("id", "prod3568").put("name", "The Coffee House").put("price", 3.99));
        addBusiness(new JsonObject().put("id", "prod7340").put("name", "Tea Cosy").put("price", 5.99));
        addBusiness(new JsonObject().put("id", "prod8643").put("name", "Spatula").put("price", 1.00));
    }

    private void addBusiness(JsonObject product) {
        products.put(product.getString("id"), product);
    }
}
