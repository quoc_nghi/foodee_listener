package com.foodee.entity;

import com.foodee.util.HashMapConverter;
import com.vividsolutions.jts.geom.Point;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;

@Entity
@Table(name = "RESTAURANT")
public class Restaurant implements java.io.Serializable {
    @Id
    @Column(name = "restaurant_id", columnDefinition = "serial")
    @GeneratedValue
    private Integer id;

    @Column(name = "factual_id")
    private String factualId;

    @Column(nullable = true)
    private String description;

    @Column(nullable = true)
    private String name;

    @Column(columnDefinition = "Point")
    private Point coordinates;

    @Column(nullable = true)
    private String address;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(nullable = true)
    private String country;

    @Column(nullable = true)
    private String locality;

    @Column(nullable = true)
    private String region;

    @Column(name = "created_date",nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @Column(name = "updated_date",nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    @Column(name = "num_of_reviews",nullable = true)
    private Integer numReviews;

    @Column(name = "web_url",nullable = true)
    private String webUrl;

    @Column(name = "phone",nullable = true)
    private String phone;

    @Column(name = "hours_display",nullable = true)
    private String hoursDisplay;

    @Column(nullable = true)
    private String email;

    @Column(name = "open_time",nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date openTime;

    @Column(name = "close_time",nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Date close_time;

    @Column(name = "additional_details",columnDefinition = "json",nullable = true)
    @Convert(converter = HashMapConverter.class)
    private HashMap<String, Double> additionalDetails;

    @Column(name = "max_price")
    private Double maxPrice;

    @Column(name = "min_price")
    private Double minPrice;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getNumReviews() {
        return numReviews;
    }

    public void setNumReviews(Integer numReviews) {
        this.numReviews = numReviews;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    public Date getClose_time() {
        return close_time;
    }

    public void setClose_time(Date close_time) {
        this.close_time = close_time;
    }

    public HashMap<String, Double> getAdditionalDetails() {
        return additionalDetails;
    }

    public void setAdditionalDetails(HashMap<String, Double> additionalDetails) {
        this.additionalDetails = additionalDetails;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public Integer getId() {
        return id;
    }

    public Point getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Point coordinates) {
        this.coordinates = coordinates;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFactualId() {
        return factualId;
    }

    public void setFactualId(String factualId) {
        this.factualId = factualId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHoursDisplay() {
        return hoursDisplay;
    }

    public void setHoursDisplay(String hoursDisplay) {
        this.hoursDisplay = hoursDisplay;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
