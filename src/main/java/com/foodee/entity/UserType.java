package com.foodee.entity;

import javax.persistence.*;

@Entity
@Table(name = "USER_TYPE")
public class UserType {
    @Id
    @Column(name = "user_type_id",columnDefinition = "serial")
    @GeneratedValue
    private Integer id;

    @Column
    private String type;

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
