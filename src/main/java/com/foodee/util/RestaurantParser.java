package com.foodee.util;

import com.foodee.entity.Restaurant;
import com.foodee.repository.RestaurantRepositoryImpl;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


public class RestaurantParser {

    public static List<Restaurant> convertJsonToRestaurant(String json) {
        JsonObject obj = new JsonObject(json);
        JsonArray data = obj.getJsonObject("response").getJsonArray("data");
        List<Restaurant> restaurants = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            Restaurant restaurant = new Restaurant();
            JsonObject restaurantJson = data.getJsonObject(i);
            restaurant.setFactualId(restaurantJson.getString("factual_id"));
            restaurant.setAddress(restaurantJson.getString("address"));
            restaurant.setCountry(restaurantJson.getString("country"));
            restaurant.setLocality(restaurantJson.getString("locality"));
            restaurant.setName(restaurantJson.getString("name"));
            restaurant.setRegion(restaurantJson.getString("region"));
            restaurant.setPhone(restaurantJson.getString("tel"));
            restaurant.setEmail(restaurantJson.getString("email"));
            restaurant.setWebUrl(restaurantJson.getString("website"));
            restaurant.setPostalCode(restaurantJson.getString("postcode"));
            restaurant.setHoursDisplay(restaurantJson.getString("hours_display"));

        }
        return restaurants;
    }

}
