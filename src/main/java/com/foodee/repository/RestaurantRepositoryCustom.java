package com.foodee.repository;

import com.foodee.entity.Restaurant;

import java.util.List;

/**
 * Created by nghibui on 30/1/16.
 */
public interface RestaurantRepositoryCustom {
    void persistListRestaurant(List<Restaurant> restaurantList);
}
