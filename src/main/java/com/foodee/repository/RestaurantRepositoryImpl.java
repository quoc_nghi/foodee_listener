package com.foodee.repository;

import com.foodee.entity.Restaurant;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import java.util.List;

@Component
@Transactional
public class RestaurantRepositoryImpl implements RestaurantRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void persistListRestaurant(List<Restaurant> restaurantList) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        for(Restaurant r : restaurantList){
            entityManager.persist(r);
        }
        tx.commit();
    }
}
