package com.foodee.repository;

import com.foodee.entity.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RestauranRepository extends JpaRepository<Restaurant, Integer> {
}
