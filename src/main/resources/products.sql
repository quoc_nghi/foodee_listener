--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.5
-- Dumped by pg_dump version 9.5beta1

-- Started on 2016-01-29 16:11:34 ICT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE foodee;
--
-- TOC entry 2379 (class 1262 OID 33383)
-- Name: foodee; Type: DATABASE; Schema: -; Owner: nghibui
--

CREATE DATABASE foodee WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


ALTER DATABASE foodee OWNER TO nghibui;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: nghibui
--

CREATE SCHEMA public;

--
-- TOC entry 2380 (class 0 OID 0)
-- Dependencies: 5
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: nghibui
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 195 (class 3079 OID 12123)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 195
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 194 (class 1259 OID 33475)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hibernate_sequence OWNER TO nghibui;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 173 (class 1259 OID 33386)
-- Name: ratingtype; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE ratingtype (
    rating_type_id integer NOT NULL,
    type character varying(255)
);


ALTER TABLE ratingtype OWNER TO nghibui;

--
-- TOC entry 172 (class 1259 OID 33384)
-- Name: ratingtype_rating_type_id_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE ratingtype_rating_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ratingtype_rating_type_id_seq OWNER TO nghibui;

--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 172
-- Name: ratingtype_rating_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE ratingtype_rating_type_id_seq OWNED BY ratingtype.rating_type_id;


--
-- TOC entry 175 (class 1259 OID 33394)
-- Name: restaurant; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE restaurant (
    restaurant_id integer NOT NULL,
    additional_details character varying(255),
    address character varying(255),
    close_time timestamp without time zone,
    coordinates character varying(255),
    created_date timestamp without time zone,
    description character varying(255),
    max_price double precision,
    min_price double precision,
    num_of_reviews integer,
    open_time timestamp without time zone,
    postal_code integer,
    updated_date timestamp without time zone,
    web_url character varying(255)
);


ALTER TABLE restaurant OWNER TO nghibui;

--
-- TOC entry 177 (class 1259 OID 33405)
-- Name: restaurant_image; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE restaurant_image (
    restaurant_image_id integer NOT NULL,
    image_url character varying(255),
    restaurant_id integer
);


ALTER TABLE restaurant_image OWNER TO nghibui;

--
-- TOC entry 176 (class 1259 OID 33403)
-- Name: restaurant_image_restaurant_image_id_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE restaurant_image_restaurant_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE restaurant_image_restaurant_image_id_seq OWNER TO nghibui;

--
-- TOC entry 2384 (class 0 OID 0)
-- Dependencies: 176
-- Name: restaurant_image_restaurant_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE restaurant_image_restaurant_image_id_seq OWNED BY restaurant_image.restaurant_image_id;


--
-- TOC entry 174 (class 1259 OID 33392)
-- Name: restaurant_restaurant_id_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE restaurant_restaurant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE restaurant_restaurant_id_seq OWNER TO nghibui;

--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 174
-- Name: restaurant_restaurant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE restaurant_restaurant_id_seq OWNED BY restaurant.restaurant_id;


--
-- TOC entry 179 (class 1259 OID 33413)
-- Name: restaurant_tag; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE restaurant_tag (
    restaurant_tag_id integer NOT NULL,
    restaurant_id integer,
    tag_id integer
);


ALTER TABLE restaurant_tag OWNER TO nghibui;

--
-- TOC entry 178 (class 1259 OID 33411)
-- Name: restaurant_tag_restaurant_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE restaurant_tag_restaurant_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE restaurant_tag_restaurant_tag_id_seq OWNER TO nghibui;

--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 178
-- Name: restaurant_tag_restaurant_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE restaurant_tag_restaurant_tag_id_seq OWNED BY restaurant_tag.restaurant_tag_id;


--
-- TOC entry 181 (class 1259 OID 33421)
-- Name: review; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE review (
    review_id integer NOT NULL,
    created_date timestamp without time zone,
    description character varying(255),
    restaurant_id integer,
    updated_date timestamp without time zone,
    user_id integer
);


ALTER TABLE review OWNER TO nghibui;

--
-- TOC entry 183 (class 1259 OID 33429)
-- Name: review_image; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE review_image (
    review_image_id integer NOT NULL,
    image_url character varying(255),
    review_id integer
);


ALTER TABLE review_image OWNER TO nghibui;

--
-- TOC entry 182 (class 1259 OID 33427)
-- Name: review_image_review_image_id_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE review_image_review_image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE review_image_review_image_id_seq OWNER TO nghibui;

--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 182
-- Name: review_image_review_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE review_image_review_image_id_seq OWNED BY review_image.review_image_id;


--
-- TOC entry 180 (class 1259 OID 33419)
-- Name: review_review_id_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE review_review_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE review_review_id_seq OWNER TO nghibui;

--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 180
-- Name: review_review_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE review_review_id_seq OWNED BY review.review_id;


--
-- TOC entry 185 (class 1259 OID 33437)
-- Name: tag; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE tag (
    tag_id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE tag OWNER TO nghibui;

--
-- TOC entry 184 (class 1259 OID 33435)
-- Name: tag_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE tag_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tag_tag_id_seq OWNER TO nghibui;

--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 184
-- Name: tag_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE tag_tag_id_seq OWNED BY tag.tag_id;


--
-- TOC entry 187 (class 1259 OID 33445)
-- Name: user_followed_restaurant; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE user_followed_restaurant (
    user_followed_restaurant integer NOT NULL,
    restaurant_id integer,
    user_id integer
);


ALTER TABLE user_followed_restaurant OWNER TO nghibui;

--
-- TOC entry 186 (class 1259 OID 33443)
-- Name: user_followed_restaurant_user_followed_restaurant_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE user_followed_restaurant_user_followed_restaurant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_followed_restaurant_user_followed_restaurant_seq OWNER TO nghibui;

--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 186
-- Name: user_followed_restaurant_user_followed_restaurant_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE user_followed_restaurant_user_followed_restaurant_seq OWNED BY user_followed_restaurant.user_followed_restaurant;


--
-- TOC entry 189 (class 1259 OID 33453)
-- Name: user_profile_image; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE user_profile_image (
    user_profile_image integer NOT NULL,
    image_url character varying(255),
    user_id integer
);


ALTER TABLE user_profile_image OWNER TO nghibui;

--
-- TOC entry 188 (class 1259 OID 33451)
-- Name: user_profile_image_user_profile_image_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE user_profile_image_user_profile_image_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_profile_image_user_profile_image_seq OWNER TO nghibui;

--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 188
-- Name: user_profile_image_user_profile_image_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE user_profile_image_user_profile_image_seq OWNED BY user_profile_image.user_profile_image;


--
-- TOC entry 191 (class 1259 OID 33461)
-- Name: user_type; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE user_type (
    user_type_id integer NOT NULL,
    type character varying(255)
);


ALTER TABLE user_type OWNER TO nghibui;

--
-- TOC entry 190 (class 1259 OID 33459)
-- Name: user_type_user_type_id_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE user_type_user_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_type_user_type_id_seq OWNER TO nghibui;

--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 190
-- Name: user_type_user_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE user_type_user_type_id_seq OWNED BY user_type.user_type_id;


--
-- TOC entry 193 (class 1259 OID 33469)
-- Name: user_viewed_restaurant; Type: TABLE; Schema: public; Owner: nghibui
--

CREATE TABLE user_viewed_restaurant (
    user_viewed_restaurant integer NOT NULL,
    restaurant_id integer,
    user_id integer
);


ALTER TABLE user_viewed_restaurant OWNER TO nghibui;

--
-- TOC entry 192 (class 1259 OID 33467)
-- Name: user_viewed_restaurant_user_viewed_restaurant_seq; Type: SEQUENCE; Schema: public; Owner: nghibui
--

CREATE SEQUENCE user_viewed_restaurant_user_viewed_restaurant_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_viewed_restaurant_user_viewed_restaurant_seq OWNER TO nghibui;

--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 192
-- Name: user_viewed_restaurant_user_viewed_restaurant_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: nghibui
--

ALTER SEQUENCE user_viewed_restaurant_user_viewed_restaurant_seq OWNED BY user_viewed_restaurant.user_viewed_restaurant;


--
-- TOC entry 2210 (class 2604 OID 33389)
-- Name: rating_type_id; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY ratingtype ALTER COLUMN rating_type_id SET DEFAULT nextval('ratingtype_rating_type_id_seq'::regclass);


--
-- TOC entry 2211 (class 2604 OID 33397)
-- Name: restaurant_id; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY restaurant ALTER COLUMN restaurant_id SET DEFAULT nextval('restaurant_restaurant_id_seq'::regclass);


--
-- TOC entry 2212 (class 2604 OID 33408)
-- Name: restaurant_image_id; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY restaurant_image ALTER COLUMN restaurant_image_id SET DEFAULT nextval('restaurant_image_restaurant_image_id_seq'::regclass);


--
-- TOC entry 2213 (class 2604 OID 33416)
-- Name: restaurant_tag_id; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY restaurant_tag ALTER COLUMN restaurant_tag_id SET DEFAULT nextval('restaurant_tag_restaurant_tag_id_seq'::regclass);


--
-- TOC entry 2214 (class 2604 OID 33424)
-- Name: review_id; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY review ALTER COLUMN review_id SET DEFAULT nextval('review_review_id_seq'::regclass);


--
-- TOC entry 2215 (class 2604 OID 33432)
-- Name: review_image_id; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY review_image ALTER COLUMN review_image_id SET DEFAULT nextval('review_image_review_image_id_seq'::regclass);


--
-- TOC entry 2216 (class 2604 OID 33440)
-- Name: tag_id; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY tag ALTER COLUMN tag_id SET DEFAULT nextval('tag_tag_id_seq'::regclass);


--
-- TOC entry 2217 (class 2604 OID 33448)
-- Name: user_followed_restaurant; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY user_followed_restaurant ALTER COLUMN user_followed_restaurant SET DEFAULT nextval('user_followed_restaurant_user_followed_restaurant_seq'::regclass);


--
-- TOC entry 2218 (class 2604 OID 33456)
-- Name: user_profile_image; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY user_profile_image ALTER COLUMN user_profile_image SET DEFAULT nextval('user_profile_image_user_profile_image_seq'::regclass);


--
-- TOC entry 2219 (class 2604 OID 33464)
-- Name: user_type_id; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY user_type ALTER COLUMN user_type_id SET DEFAULT nextval('user_type_user_type_id_seq'::regclass);


--
-- TOC entry 2220 (class 2604 OID 33472)
-- Name: user_viewed_restaurant; Type: DEFAULT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY user_viewed_restaurant ALTER COLUMN user_viewed_restaurant SET DEFAULT nextval('user_viewed_restaurant_user_viewed_restaurant_seq'::regclass);


--
-- TOC entry 2394 (class 0 OID 0)
-- Dependencies: 194
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('hibernate_sequence', 1, false);


--
-- TOC entry 2353 (class 0 OID 33386)
-- Dependencies: 173
-- Data for Name: ratingtype; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2395 (class 0 OID 0)
-- Dependencies: 172
-- Name: ratingtype_rating_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('ratingtype_rating_type_id_seq', 1, false);


--
-- TOC entry 2355 (class 0 OID 33394)
-- Dependencies: 175
-- Data for Name: restaurant; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2357 (class 0 OID 33405)
-- Dependencies: 177
-- Data for Name: restaurant_image; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2396 (class 0 OID 0)
-- Dependencies: 176
-- Name: restaurant_image_restaurant_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('restaurant_image_restaurant_image_id_seq', 1, false);


--
-- TOC entry 2397 (class 0 OID 0)
-- Dependencies: 174
-- Name: restaurant_restaurant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('restaurant_restaurant_id_seq', 1, false);


--
-- TOC entry 2359 (class 0 OID 33413)
-- Dependencies: 179
-- Data for Name: restaurant_tag; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2398 (class 0 OID 0)
-- Dependencies: 178
-- Name: restaurant_tag_restaurant_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('restaurant_tag_restaurant_tag_id_seq', 1, false);


--
-- TOC entry 2361 (class 0 OID 33421)
-- Dependencies: 181
-- Data for Name: review; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2363 (class 0 OID 33429)
-- Dependencies: 183
-- Data for Name: review_image; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2399 (class 0 OID 0)
-- Dependencies: 182
-- Name: review_image_review_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('review_image_review_image_id_seq', 1, false);


--
-- TOC entry 2400 (class 0 OID 0)
-- Dependencies: 180
-- Name: review_review_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('review_review_id_seq', 1, false);


--
-- TOC entry 2365 (class 0 OID 33437)
-- Dependencies: 185
-- Data for Name: tag; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2401 (class 0 OID 0)
-- Dependencies: 184
-- Name: tag_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('tag_tag_id_seq', 1, false);


--
-- TOC entry 2367 (class 0 OID 33445)
-- Dependencies: 187
-- Data for Name: user_followed_restaurant; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2402 (class 0 OID 0)
-- Dependencies: 186
-- Name: user_followed_restaurant_user_followed_restaurant_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('user_followed_restaurant_user_followed_restaurant_seq', 1, false);


--
-- TOC entry 2369 (class 0 OID 33453)
-- Dependencies: 189
-- Data for Name: user_profile_image; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2403 (class 0 OID 0)
-- Dependencies: 188
-- Name: user_profile_image_user_profile_image_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('user_profile_image_user_profile_image_seq', 1, false);


--
-- TOC entry 2371 (class 0 OID 33461)
-- Dependencies: 191
-- Data for Name: user_type; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2404 (class 0 OID 0)
-- Dependencies: 190
-- Name: user_type_user_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('user_type_user_type_id_seq', 1, false);


--
-- TOC entry 2373 (class 0 OID 33469)
-- Dependencies: 193
-- Data for Name: user_viewed_restaurant; Type: TABLE DATA; Schema: public; Owner: nghibui
--



--
-- TOC entry 2405 (class 0 OID 0)
-- Dependencies: 192
-- Name: user_viewed_restaurant_user_viewed_restaurant_seq; Type: SEQUENCE SET; Schema: public; Owner: nghibui
--

SELECT pg_catalog.setval('user_viewed_restaurant_user_viewed_restaurant_seq', 1, false);


--
-- TOC entry 2222 (class 2606 OID 33391)
-- Name: ratingtype_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY ratingtype
    ADD CONSTRAINT ratingtype_pkey PRIMARY KEY (rating_type_id);


--
-- TOC entry 2226 (class 2606 OID 33410)
-- Name: restaurant_image_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY restaurant_image
    ADD CONSTRAINT restaurant_image_pkey PRIMARY KEY (restaurant_image_id);


--
-- TOC entry 2224 (class 2606 OID 33402)
-- Name: restaurant_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY restaurant
    ADD CONSTRAINT restaurant_pkey PRIMARY KEY (restaurant_id);


--
-- TOC entry 2228 (class 2606 OID 33418)
-- Name: restaurant_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY restaurant_tag
    ADD CONSTRAINT restaurant_tag_pkey PRIMARY KEY (restaurant_tag_id);


--
-- TOC entry 2232 (class 2606 OID 33434)
-- Name: review_image_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY review_image
    ADD CONSTRAINT review_image_pkey PRIMARY KEY (review_image_id);


--
-- TOC entry 2230 (class 2606 OID 33426)
-- Name: review_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY review
    ADD CONSTRAINT review_pkey PRIMARY KEY (review_id);


--
-- TOC entry 2234 (class 2606 OID 33442)
-- Name: tag_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (tag_id);


--
-- TOC entry 2236 (class 2606 OID 33450)
-- Name: user_followed_restaurant_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY user_followed_restaurant
    ADD CONSTRAINT user_followed_restaurant_pkey PRIMARY KEY (user_followed_restaurant);


--
-- TOC entry 2238 (class 2606 OID 33458)
-- Name: user_profile_image_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY user_profile_image
    ADD CONSTRAINT user_profile_image_pkey PRIMARY KEY (user_profile_image);


--
-- TOC entry 2240 (class 2606 OID 33466)
-- Name: user_type_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY user_type
    ADD CONSTRAINT user_type_pkey PRIMARY KEY (user_type_id);


--
-- TOC entry 2242 (class 2606 OID 33474)
-- Name: user_viewed_restaurant_pkey; Type: CONSTRAINT; Schema: public; Owner: nghibui
--

ALTER TABLE ONLY user_viewed_restaurant
    ADD CONSTRAINT user_viewed_restaurant_pkey PRIMARY KEY (user_viewed_restaurant);


--
-- TOC entry 2381 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: nghibui
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM nghibui;
GRANT ALL ON SCHEMA public TO nghibui;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-01-29 16:11:34 ICT

--
-- PostgreSQL database dump complete
--

